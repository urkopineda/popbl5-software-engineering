package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.Definitions;
import frontend.DebuggingUI;

/**
 * Receive
 * 
 * This class, using a created socket, receives data throught sockets.
 * 
 * @author Skynet Team
 *
 */
public class Receive extends Thread {

	private static final Logger log = Logger.getLogger(Receive.class.getName());
	private Buzon<String> buzon;
	private Socket socket;
	private BufferedReader input = null;
	private boolean stop = false;
	private DebuggingUI dUI;
	
	/**
	 * The constructor receives the "Socket" object and a mailbox to place the new messages received.
	 */
	protected Receive(Socket socket, Buzon<String> buzon, Definitions definitions) {
		this.socket = socket;
		this.buzon = buzon;
		dUI = new DebuggingUI("Receive", definitions);
	}
	
	/**
	 * This methods creates a "BufferedReader" object with the "Socket.getInputStream()". Then, the thread
	 * waits for a new message in the sockte and, when the thread receives a message, it places the message
	 * in a String mailbox.
	 */
	@Override
	public void run() {
		try {
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			log.log(Level.SEVERE, "ERROR: Ha ocurrido un problema con el socket.", e);
		}
		while (!stop) {
			dUI.print("RECEIVE: Esperando mensajes... ");
			char [] data = null;
			try {
				ArrayList<Character> a = new ArrayList<>(); 
				int value;
				while ((value = input.read()) != 3) {
					a.add((char) value);
				}
				data = new char[a.size()];
				for (int i = 0; i < a.size(); i++) {
					data[i] = a.get(i);
				}
			} catch (IOException e) {
                log.log(Level.WARNING, "Conexion cerrada por el servidor.", e);
			}
			if (data != null) {
				try {
					buzon.send(String.valueOf(data));
					dUI.println("Recibido!");
				} catch (InterruptedException e) {
                    log.log(Level.WARNING, "Interrupted!", e);
                    Thread.currentThread().interrupt();
                }
			}
		}
        dUI.println("RECEIVE: Fin del hilo.");
	}
	
	protected void kill() {
		stop = true;
		try {
			input.close();
		} catch (IOException e) {
            log.log(Level.WARNING, "Conexion cerrada por el servidor.", e);
		}
	}
	
}
