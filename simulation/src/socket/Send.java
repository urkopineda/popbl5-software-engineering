package socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.Definitions;
import frontend.DebuggingUI;

/**
 * Send
 * 
 * Send is the class that creates a thread to communicate throught sokect.
 * This class sends the data to the base.
 * 
 * @author Skynet Team
 *
 */
public class Send extends Thread {

    private static final Logger log = Logger.getLogger(Send.class.getName());
	private Buzon<Message> buzon;
	private Socket socket;
	private PrintWriter output = null;
	private boolean stop = false;
	private DebuggingUI dUI;
	
	/**
	 * The constructor receives the "Socket" object and a mailbox to listen for new messages to send.
	 */
	protected Send(Socket socket, Buzon<Message> buzon, Definitions definitions) {
		this.socket = socket;
		this.buzon = buzon;
		dUI = new DebuggingUI("Send", definitions);
	}
	
	/**
	 * This methods creates a "PrintWriter" object with the "Socket.getOutputStream()". Then, the thread
	 * waits for a new message in the mailbox and, when the thread takes it out the mailbox, sends it
	 * using the PrintWriter and finnally flushs it to the base.
	 */
	@Override
	public void run() {
		try {
			output = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			log.log(Level.SEVERE, "ERROR: Ha ocurrido un problema con el socket.", e);
		}
		while (!stop) {
			dUI.print("SEND: Esperando datos... ");
			try {
				Message msg = buzon.receive();
				dUI.print(msg.toString());
				output.print(msg.toSocketFormat());
			} catch (InterruptedException e) {
                log.log(Level.WARNING, "Interrupted!", e);
                Thread.currentThread().interrupt();
			}
			output.flush();
			dUI.println(" Enviado!");
		}
        dUI.println("SEND: Fin del hilo");
	}
	
	protected void kill() {
		stop = true;
		output.close();
	}
	
}
