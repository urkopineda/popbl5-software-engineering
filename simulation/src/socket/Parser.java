package socket;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data.Definitions;
import frontend.DebuggingUI;

/**
 * Parser 
 * 
 * Parser class takes a received message with a concrete format and, parsing the Message, creates a "Message" object.
 * 
 * @author Skynet Team
 *
 */
public class Parser extends Thread {

    private static final Logger log = Logger.getLogger(Parser.class.getName());
    private Matcher matcher;
	private static final String REGEX = "^\u0002?([0-9]+)\u001D?(.*)$";
	private Buzon<String> in;
	private Buzon<Message> out;
	private boolean stop = false;
	private DebuggingUI dUI;
	
	/**
	 * The constructor sets the in and out mailbox.
	 * 
	 * @param in Buzon in mailbox.
	 * @param out Buzon out mailbox.
	 */
	public Parser(Buzon<String> in, Buzon<Message> out, Definitions definitions) {
		this.in = in;
		this.out = out;
		dUI = new DebuggingUI("Parser", definitions);
	}
	
	/**
	 * Parser compares the received message, checks the format and, if its correct,
	 * divides the message and creates a "Message" objects. Finally, it places the
	 * "Message" object in the output mailbox.
	 */
	@Override
	public void run() {
		while (!stop) {
			dUI.print("PARSER: Esperando mensaje... ");
			Message msg = parseMessage(receive());
            if (msg != null) {
                send(msg);
            }
		}
        dUI.println("PARSER: Fin del hilo.");
	}

	public Message parseMessage(String message) {
		matcher = Pattern.compile(REGEX).matcher(message);
		dUI.println("OK!");
		if (matcher.matches()) {
			dUI.print("PARSER: Parseando mensaje... ");
			int id = matcher.group(1) != null ? Integer.parseInt(matcher.group(1)): -1;
			String data = matcher.group(2) != null ? matcher.group(2) : "";
			String [] d = data.split("\u001D");
            d[1] = d[1].split("\u0003")[0];
			Message msg = new Message(id, d[0], d[1]);
			dUI.print(msg.toString());
			dUI.println(" OK!");
            return msg;
		} else {
			dUI.println("PARSER - ERROR: El formato del mensaje no es correcto.");
            return null;
		}
	}
	
	/**
	 * Receives the string message throught the input mailbox.
	 * 
	 * @return line String message text.
	 */
	private String receive() {
		String line = null;
		try {
			line = in.receive();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
		return line;
	}
	
	/**
	 * Sends the message to the output mailbox.
	 * 
	 * @param message Message receiverd new message.
	 */
	private void send(Message message) {
		try {
			out.send(message);
		} catch (InterruptedException e) {
            log.log(Level.WARNING, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
	}
	
	public void kill() {
		stop = true;
	}

}
