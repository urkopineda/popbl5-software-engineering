package socket;

import data.Definitions;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Multicast
 * 
 * This class creates a Multicast socket to listen to a multicast group.
 * 
 * @author Skynet Team
 *
 */
public class Multicast extends Thread {

	private static final Logger log = Logger.getLogger(Multicast.class.getName());
	private Buzon<String> buzon;
	private MulticastSocket socket;
	private InetAddress group;
	private Definitions definitions;
	private boolean stop = false;
	
	/**
	 * Multicast constructor receives a mailbox to place the alert messages.
	 * 
	 * @param buzon Buzon buzon (mailbox)
	 */
	public Multicast(Buzon<String> buzon, Definitions definitions) {
		this.buzon = buzon;
		this.definitions = definitions;
		init();
	}
	
	/**
	 * After joining the group, starts listening to the multicast socket.
	 */
	@Override
	public void run() {
		DatagramPacket packet;
		while (!stop) {
			byte [] buf = new byte [256];
		    packet = new DatagramPacket(buf, buf.length);
		    try {
				socket.receive(packet);
			} catch (IOException e) {
				log.log(Level.WARNING, "ERROR: Socket package error.", e);
			}
		    try {
		    	String s = new String(packet.getData());
				buzon.send(s);
			} catch (InterruptedException e) {
                log.log(Level.WARNING, "Interrupted!", e);
                Thread.currentThread().interrupt();
			}
		}
	}
	
	/**
	 * This method joins the Multicast group.
	 */
	private void init() {
		try {
			socket = new MulticastSocket(definitions.getMulticastPort());
			group = InetAddress.getByName(definitions.getMulticastGroup());
			socket.joinGroup(group);
		} catch (IOException e) {
            log.log(Level.WARNING, "ERROR: Socket error.", e);
		}
	}
	
	/**
	 * This method leaves the group.
	 */
	public void close() {
		stop = true;
		try {
			socket.leaveGroup(group);
		} catch (IOException e) {
            log.log(Level.WARNING, "ERROR: Socket error.", e);
		}
		socket.close();
	}

}
