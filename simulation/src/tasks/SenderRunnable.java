package tasks;

import data.Definitions;
import socket.Dispatcher;

/**
 * SenderRunnable
 * 
 * SenderRunnable sends the location of the resource each X seconds.
 * 
 * @author Skynet Team
 *
 */
public class SenderRunnable extends Thread {
	
	private Definitions definitions;
	private Dispatcher d;
	private String type;
	
	public SenderRunnable(Definitions definitions, Dispatcher d, String type) {
		this.definitions = definitions;
		this.d = d;
		this.type = type;
	}
	
	@Override
	public void run() {
		String data = definitions.getLat() + ", " + definitions.getLng();
		if (((int) definitions.getLat() != -1) && ((int) definitions.getLng() != -1)) {
			d.send(definitions.getId(), type, data);
		}
	}

}
