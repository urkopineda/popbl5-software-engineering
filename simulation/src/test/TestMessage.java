package test;

import org.junit.*;
import org.junit.Test;
import socket.Message;

import static org.junit.Assert.assertEquals;

/**
 * Created by urko on 30/04/16.
 */
public class TestMessage {

    public Message message;

    @Before
    public void before() {
        message = new Message(0, "A", "B");
    }

    @Test
    public void testGetData() {
        assertEquals(message.getData(), "B");
    }

    @Test
    public void testGetType() {
        assertEquals(message.getType(), "A");
    }

    @Test
    public void testGetId() {
        assertEquals(message.getID(), 0);
    }

}
