package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by urko on 30/04/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({TestParser.class, TestBuzon.class, TestMessage.class})
public class Test {}
