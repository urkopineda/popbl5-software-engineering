package test;

import data.Definitions;
import org.junit.Before;
import org.junit.Test;
import socket.Buzon;
import socket.Message;
import socket.Parser;

import static org.junit.Assert.assertEquals;

/**
 * Created by urko on 30/04/16.
 */
public class TestParser {

    public Parser parser;
    public Buzon<String> in;
    public Buzon<Message> out;
    public Definitions definitions;

    @Before
    public void before() {
        definitions = new Definitions();
        definitions.setDebugging(false);
        in = new Buzon<>(10);
        out = new Buzon<>(10);
        parser = new Parser(in, out, definitions);
    }

    @Test
    public void parserTest() {
        Message message = new Message(0, "A", "B");
        System.out.println(message.toSocketFormat());
        assertEquals(parser.parseMessage(message.toSocketFormat()).toSocketFormat(), message.toSocketFormat());
    }

}
