package test;

import org.junit.*;
import org.junit.Test;
import socket.Buzon;
import socket.Message;

import static org.junit.Assert.assertEquals;

/**
 * Created by urko on 30/04/16.
 */
public class TestBuzon {

    public Buzon<Object> buzon;

    @Before
    public void before() {
        buzon = new Buzon<>(1);
    }

    @Test
    public void testSend() throws InterruptedException {
        buzon.send();
        assertEquals(buzon.getSize(), 1);
    }

    @Test
    public void testSendObject() throws InterruptedException {
        buzon.send(new Object());
        assertEquals(buzon.getSize(), 1);
    }

    @Test
    public void testReceive() throws InterruptedException {
        String text = "Hi!";
        buzon.send(text);
        assertEquals(buzon.receive(), text);
    }

    @Test
    public void testReceiveMesage() throws InterruptedException {
        Message message = new Message(0, "A", "B");
        buzon.send(message);
        assertEquals(buzon.receive("A"), message);
    }

}
