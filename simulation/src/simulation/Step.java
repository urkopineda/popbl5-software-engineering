package simulation;

/**
 * Step
 * 
 * This class storages the information of a step to navigate in Google Maps.
 * 
 * @author Skynet Team
 *
 */
public class Step {

	private int distanceV;
	private int durationV;
	private double startLat;
	private double startLng;
	private String instruction;

	public int getDistanceV() {
		return distanceV;
	}

	public void setDistanceV(int distanceV) {
		this.distanceV = distanceV;
	}

	public int getDurationV() {
		return durationV;
	}

	public void setDurationV(int durationV) {
		this.durationV = durationV;
	}

	public double getStartLat() {
		return startLat;
	}

	public void setStartLat(double startLat) {
		this.startLat = startLat;
	}

	public double getStartLng() {
		return startLng;
	}

	public void setStartLng(double startLng) {
		this.startLng = startLng;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	
	@Override
	public String toString() {
		return getInstruction();
	}

}
