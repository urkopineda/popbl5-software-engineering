package simulation;

import java.util.ArrayList;
import java.util.List;

/**
 * Navigation
 * 
 * This class is used by the "Simulation" class to simulate a route on Google Maps.
 * 
 * @author Skynet Team
 *
 */
public class Navigation {

	private String distance;
	private String duration;
	
	private List<Step> steps;
	
	public Navigation() {
		steps = new ArrayList<>();
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public List<Step> getSteps() {
		return steps;
	}
	
	public void addStep(Step step) {
		steps.add(step);
	}
	
}
