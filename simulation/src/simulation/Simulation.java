package simulation;

import data.Resource;
import frontend.MainUI;
import json.JSON;
import org.json.JSONException;
import socket.Buzon;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simulation
 * 
 * This class creates a thread to simulate the navigation route in Google Maps.
 * 
 * @author Skynet Team
 *
 */
public class Simulation extends Thread {

    private static final Logger log = Logger.getLogger(Simulation.class.getName());
	private MainUI ui;
	private Buzon<String> buzon;
	private Resource r;
	
	private Navigation nav;
	private List<Step> steps;
	
	private static final int STATIC_STEPS = 100;
	private boolean stop = false;
	
	private boolean driving = false;
	
	/**
	 * The constructo set the needed varibles for the Simulation.
	 *  
	 * @param ui MainUI ui is needed to print the route in the UI.
	 * @param buzon Buzon buzon (mailbox) is needed to listen for new routes.
	 * @param r Resource r is needed to change the location and state of the resource.
	 */
	public Simulation(MainUI ui, Buzon<String> buzon, Resource r) {
		this.ui = ui;
		this.buzon = buzon;
		this.r = r;
	}
	
	/**
	 * The thread starts listening to the mailbox. When the thread receives a JSON, it parses it using
	 * "JSON" object, then sets the state of the resource and starts the simulation. The simulation will
	 * be stopped by the "Resource" thread if a route is received when the state is 1. If the simulation
	 * finishes the route to the incident, the incident will be resolved and the resource will be changed
	 * to the state 2. When the server receives the state 2 message, the server will send a JSON with the
	 * return home path.
	 */
	@Override
	public void run() {
		while (!stop) {
			JSON json = null;
			int result = 0;
			try {
				json = new JSON(buzon.receive());
				result = json.parseNav();
			} catch (InterruptedException e) {
                log.log(Level.WARNING, "Interrupted!", e);
                Thread.currentThread().interrupt();
			} catch (JSONException e) {
                log.log(Level.SEVERE, "JSON error", e);
				result = -1;
			}
			if (result < 0) {
				r.setEstado(0);
			} else {
				driving(json);
			}
		}
	}

    private void driving(JSON json) {
        driving = true;
        try {
            nav = json.getNav();
        } catch (NullPointerException e) {
            log.log(Level.SEVERE, "JSON error", e);
        }
        steps = nav.getSteps();
        if (r.getEstado() == 0) {
            r.setEstado(1);
            ui.addNavText("Ruta (ida) recibida, leyendo resumen de ruta.");
        } else if (r.getEstado() == 2) {
            ui.addNavText("Ruta (vuelta) recibida, leyendo resumen de ruta.");
        }
        ui.setDistanciaTotal(nav.getDistance());
        ui.setDuracionTotal(nav.getDuration());
        ui.addNavText("Leyendo instrucciones de ruta:");
        addPointer(steps.get(0).getStartLat(), steps.get(0).getStartLng());
        ui.addNavText("\tPASO 0");
        ui.addNavText("\t\t"+steps.get(0).getInstruction());
        for (int i = 1; i < steps.size(); i++) {
            updatePointer(steps.get(i).getStartLat(), steps.get(i).getStartLng());
            ui.addNavText("\tPASO "+i);
            ui.addNavText("\t\t"+steps.get(i).getInstruction());
            ui.setDistanciaRest(String.valueOf(steps.get(i).getDistanceV() * 0.001)+" km");
            ui.setDuracionRest(secondsToString(steps.get(i).getDurationV()));
            cancelRoute();
        }
        if (r.getEstado() != 2) {
            r.setEstado(2);
            ui.addNavText("Incidente atendido, esperando ruta de vuelta.");
        } else {
            r.setEstado(0);
            ui.addNavText("Ruta de vuelta finalizada.");
            ui.clearNavText();
            ui.addNavText("Esperando ruta...");
        }
    }

    private void cancelRoute() {
        if ((stop) && (r.getEstado() == 1)) {
            r.setEstado(0);
            ui.clearNavText();
            ui.addNavText("Ruta cancelada, nueva ruta recibida.");
            return;
        }
    }

	public void kill() {
		this.interrupt();
		stop = true;
	}
	
	/**
	 * This method add a pointer to the map and updates the location of the resource.
	 */
	private void addPointer(double lat, double lng) {
		r.setLocation(lat, lng);
		ui.addPointer(lat, lng);
	}
	
	/**
	 * Updates the position of the marker in a progressive way to simulate the movement
	 * of a resource.
	 */
	private void updatePointer(double lat, double lng) {
		double diffLat = lat - r.getLat();
		double diffLng = lng - r.getLng();
		double length = Math.sqrt((diffLat * diffLat) + (diffLng * diffLng)) * STATIC_STEPS;
		int j = (int) length;
		for (int i = 0; i < j; i++) {
			double oLat = r.getLat() + (diffLat / j);
			double oLng = r.getLng() + (diffLng / j);
			r.setLocation(oLat, oLng);
			ui.updatePointer(oLat, oLng);
			if (stop) {
                return;
            }
		}
	}
	
	private String secondsToString(int val) {
		int hours = val / 3600;
	    int remainder = val - hours * 3600;
	    int mins = remainder / 60;
		return hours+"h "+mins+"min";
	}
	
	public boolean getDriving() {
		return driving;
	}

}
