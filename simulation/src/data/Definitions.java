package data;

/**
 * Definitions
 * 
 * This class is used to share global variables between different classes.
 * 
 * @author Skynet Team
 *
 */
public class Definitions {

    private boolean debugging = true;
    private String configFile = "conf.dat";
    private int socketNumber;
    private String socketAddres;
    private int id = -1;
    private int estado = 0;
    private double lat = 43.063081;
    private double lng = -2.505862;
    private int multicastPort = 8000;
    private String multicastGroup;

	public Definitions() {
        debugging = true;
        configFile = "conf.dat";
        socketNumber = 0;
        id = -1;
        estado = 0;
        lat = 43.063081;
        lng = -2.505862;
        multicastPort = 8000;
    }

    public boolean isDebugging() {
        return debugging;
    }

    public void setDebugging(boolean debugging) {
        this.debugging = debugging;
    }

    public String getConfigFile() {
        return configFile;
    }

    public int getSocketNumber() {
        return socketNumber;
    }

    public void setSocketNumber(int socketNumber) {
        this.socketNumber = socketNumber;
    }

    public String getSocketAddres() {
        return socketAddres;
    }

    public void setSocketAddres(String socketAddres) {
        this.socketAddres = socketAddres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getMulticastPort() {
        return multicastPort;
    }

    public String getMulticastGroup() {
        return multicastGroup;
    }

    public void setMulticastGroup(String multicastGroup) {
        this.multicastGroup = multicastGroup;
    }

}
