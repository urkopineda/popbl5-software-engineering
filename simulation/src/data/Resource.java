package data;

import frontend.MainUI;
import simulation.Simulation;
import socket.Buzon;
import socket.Dispatcher;
import socket.Message;
import socket.Multicast;
import tasks.SenderRunnable;
import utils.ConfigFile;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Resource
 * 
 * Resource is the class that manage all the data of the simulation.
 * NOTE: Setters in this class rewrite config file using "rewrite()" method.
 * 
 * @author Skynet Team
 *
 */
public class Resource extends Thread {

    private static final Logger log = Logger.getLogger(Resource.class.getName());
	private MainUI ui;
	private Dispatcher d;
	private Buzon<String> simuBuzon;
	private Simulation simu;
	private ScheduledExecutorService scheduler;
	private Multicast m;
	private boolean stop = false;
    private Definitions definitions;
	
	public Resource(Dispatcher d, Definitions definitions) {
		this.d = d;
        this.definitions = definitions;
		init();
		ui = new MainUI(definitions);
		ui.start();
		simuBuzon = new Buzon<>(1000);
		prepareSimulator();
		initSimulator();
	}
	
	/**
	 * When the resource thread is running, set's UI variables to be shown to the user.
	 * Then creates the task running at the background periodically (LOCATION is send each 10 seconds)
	 * Finnally, start's listening to the mailbox that is storaged the messages.
	 */
	@Override
	public void run() {
		ui.setURL(definitions.getSocketAddres());
		ui.setSocket(definitions.getSocketNumber());
		ui.setLocation(definitions.getLat(), definitions.getLng());
		ui.setEstado(definitions.getEstado());
		ui.setID(definitions.getId());
		createTasks();
		while (!stop) {
			Message msg = d.receive();
			switch (msg.getType()) {
				case "ALERT": ui.addAlertText(msg.getData());
					break;
				case "ROUTE": doRoute(msg);
                    break;
				case "IDASSIGN": setID(msg.getID());
					break;
				case "JOIN": joinMultiCast(msg);
                    break;
				default: return;
			}
		}
	}

    private void doRoute(Message msg) {
        if ((simu != null) && (simu.getDriving())) {
            ui.reload();
            killSimulator();
            prepareSimulator();
            initSimulator();
        }
        try {
            simuBuzon.send(msg.getData());
        } catch (InterruptedException e) {
            log.log(Level.WARNING, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
    }

    private void joinMultiCast(Message msg) {
        definitions.setMulticastGroup(msg.getData());
        if ((m != null) && (m.isAlive())) {
            m.close();
        }
        m = new Multicast(d.getParserBuzon(), definitions);
        m.start();
    }
	
	/**
	 * This method initialize the resource asking for an ID to the server. If the ID is storaged in the configuration file,
	 * then restores it and set's the resource of the state to 0 if it was waiting or moving to an incident or; if it was
	 * going back to the base, sends a state 2 to the server to receive the return home route.
	 */
	private void init() {
		if (definitions.getId() == -1) {
			d.send(definitions.getId(), "IDREQUEST", " ");
			setID(d.receive("IDASSIGN").getID());
		}
        d.send(definitions.getId(), "CONNECTED", " ");
		if (definitions.getEstado() == 2) {
			setEstado(2);
		} else if (definitions.getEstado() == 1) {
			setEstado(0);
		}
	}
	
	public int getResouceId() {
		return definitions.getId();
	}
	
	private void setID(int id) {
		if (ui != null) {
            ui.setID(id);
        }
		definitions.setId(id);
		rewrite();
	}
	
	public int getEstado() {
		return definitions.getEstado();
	}

	public void setEstado(int estado) {
		if (ui != null) {
            ui.setEstado(estado);
        }
		definitions.setEstado(estado);
		d.send(definitions.getId(), "ESTADO", estado);
		rewrite();
	}
	
	public double getLat() {
		return definitions.getLat();
	}
	
	public double getLng() {
		return definitions.getLng();
	}
	
	public void setLocation(double lat, double lng) {
		definitions.setLat(lat);
		definitions.setLng(lng);
		ui.setLocation(lat, lng);
		rewrite();
	}

	/**
	 * This methods creates a periodical runnable method to send specific data to the server periodically.
	 */
	private void createTasks() {
		scheduler = Executors.newScheduledThreadPool(1);
        Runnable runnable = new SenderRunnable(definitions, d, "LOCATION");
		scheduler.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);
	}
	
	private void prepareSimulator() {
		simu = new Simulation(ui, simuBuzon, this);
	}
	
	private void initSimulator() {
		simu.start();
	}
	
	private void killSimulator() {
		simu.kill();
	}
	
	/**
	 * Rewrite calls "writeAll()" method from "ConfigFile" to write the data with the variables in "Definitions" class.
	 */
	private void rewrite() {
		ConfigFile file = new ConfigFile(definitions);
		file.writeAll();
	}
	
}
