package main;

import data.Definitions;
import data.Resource;
import frontend.OptionsUI;
import socket.Dispatcher;
import utils.ConfigFile;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Principal
 * 
 * The objective of this Simulation project is to simulate a resource. To do that, we receive information from sokects,
 * and simulate the navigation to incidents using Google Maps and JSONs.
 * 
 * @author Skynet Team
 *
 */
public class Principal {

    private static final Logger log = Logger.getLogger(Principal.class.getName());
    private Resource r;
	private Dispatcher d;
	private OptionsUI initUI;
		
	private void start(Definitions definitions) {
		initConfiUI(definitions);
		initDispatcher(definitions);
		initResource(definitions);
	}
	
	/**
	 * Starts the OptionsUI window to set the simulation variables.
	 */
	private void initConfiUI(Definitions definitions) {
		Semaphore stop =  new Semaphore(0);
		initUI = new OptionsUI(stop, definitions);
		initUI.start();
		try {
			stop.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, "Interrupted!", e);
            Thread.currentThread().interrupt();
        }
	}
	
	/**
	 * Dispatcher starts the whole system of socket communications.
	 */
	private void initDispatcher(Definitions definitions) {
		d = new Dispatcher(definitions);
		d.start();
	}
	
	/**
	 * Creates the Resource object with the Dispatcher.
	 */
	private void initResource(Definitions definitions) {
		r = new Resource(d, definitions);
		r.start();
	}
	
	public static void main(String [] args) {
        log.log(Level.INFO, "PRINCIPAL: Inicio del programa.");
		// Read the file if exists.
        Definitions definitions = new Definitions();
		ConfigFile file = new ConfigFile(definitions);
		if (file.check()) {
            file.readAll();
        }
		// Start the program.
		Principal p = new Principal();
		p.start(definitions);
        log.log(Level.INFO, "PRINCIPAL: Fin del hilo principal del programa.");
	}

}
