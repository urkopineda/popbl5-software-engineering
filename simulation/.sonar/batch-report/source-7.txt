package frontend;

import data.Definitions;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MainUI
 * 
 * This class will manage the main UI interface. This UI will be initialize by the "Resoruce" class.
 *  
 * @author Skynet Team
 *
 */
public class MainUI extends Thread {

    private static final Logger log = Logger.getLogger(MainUI.class.getName());
    private JFrame window;
	private JTextArea alertArea;
	private JTextArea navArea;
	private JScrollPane scrollAlert;
	private JScrollPane scrollNav;
	private JLabel lID;
    private JLabel lEstado;
    private JLabel lLat;
    private JLabel lLng;
    private JLabel lURL;
    private JLabel lSocket;
	private JLabel lDistanciaTotal;
    private JLabel lDistanciaRest;
    private JLabel lDuracionTotal;
    private JLabel lDuracionRest;
	private WebEngine webEngine;
	private JFXPanel panel;
	private Semaphore stop;
	private Semaphore candado;
	private boolean loaded = false;
	private Definitions definitions;
	private String mapURL = "file:///"+new File("map.html").getAbsolutePath();
    private static final String INTERRUPTED = "Interrupted!";
	
	/** 
	 * Constructor creates the Main Frame and creates the needed semaphores.
	 * Additionally, the method creates the Scene (The JavaFX panel that contains the WebView)
	 */
	public MainUI(Definitions definitions) {
		this.definitions = definitions;
		window = new JFrame();
		window.setTitle("Panel de control de Recurso");
		window.setSize(300, 200);
		window.getContentPane().add(createMainPanel());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		window.setVisible(true);
		createScene();
		stop = new Semaphore(0);
		candado = new Semaphore(1);
	}
	
	/**
	 * At start, the UI sets the JLabels and loads the map in the WebView.
	 */
	@Override
	public void run() {
		resetNav();
		addNavText("Esperando ruta...");
		loadURL();
		updateMap(definitions.getLat(), definitions.getLng());
	}
	
	/**
	 * Main panel is a BorderLayout panel with another 3 panels located in the north, center and south of the UI.
	 * 
	 * @return The main panel of the MainUI
	 */
	private Container createMainPanel() {
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(createNorthPanel(), BorderLayout.NORTH);
		mainPanel.add(mapView(), BorderLayout.CENTER);
		mainPanel.add(createSouthPanel(), BorderLayout.SOUTH);
		return mainPanel;
	}
	
	/**
	 * The north panel creates all the JLabels to show the information to the user.
	 * 
	 * @return The north panel of the MainUI
	 */
	private Container createNorthPanel() {
		JPanel northPanel = new JPanel(new GridLayout(1, 5, 0, 0));
		JPanel northPanel1 = new JPanel(new BorderLayout());
		northPanel1.setBorder(BorderFactory.createTitledBorder("ID del Recurso"));
		lID = new JLabel("--", SwingConstants.CENTER);
		northPanel1.add(lID);
		JPanel northPanel2 = new JPanel(new BorderLayout());
		northPanel2.setBorder(BorderFactory.createTitledBorder("Estado del Recurso"));
		lEstado = new JLabel("--", SwingConstants.CENTER);
		northPanel2.add(lEstado);
		JPanel northPanel3 = new JPanel(new BorderLayout());
		northPanel3.setBorder(BorderFactory.createTitledBorder("Localizacion del Recurso"));
		lLat = new JLabel("--", SwingConstants.CENTER);
		northPanel3.add(lLat, BorderLayout.NORTH);
		lLng = new JLabel("--", SwingConstants.CENTER);
		northPanel3.add(lLng, BorderLayout.CENTER);
		JPanel northPanel4 = new JPanel(new BorderLayout());
		northPanel4.setBorder(BorderFactory.createTitledBorder("IP del Servidor"));
		lURL = new JLabel("--", SwingConstants.CENTER);
		northPanel4.add(lURL);
		JPanel northPanel5 = new JPanel(new BorderLayout());
		northPanel5.setBorder(BorderFactory.createTitledBorder("Nº de Puerto"));
		lSocket = new JLabel("--", SwingConstants.CENTER);
		northPanel5.add(lSocket);
		northPanel.add(northPanel1);
		northPanel.add(northPanel2);
		northPanel.add(northPanel3);
		northPanel.add(northPanel4);
		northPanel.add(northPanel5);
		return northPanel;
	}
	
	/**
	 * South panel fills the panel (GridLayout) with a combination of panels to show alert and route information.
	 * 
	 * @return The south panel of the MainUI
	 */
	private Container createSouthPanel() {
		JPanel southPanel = new JPanel(new GridLayout(1, 2, 0, 0));
		JPanel southPanel1 = new JPanel(new BorderLayout());
		southPanel1.setBorder(BorderFactory.createTitledBorder("Mensajes de alerta"));
		alertArea = new JTextArea(12, 20);
		scrollAlert = new JScrollPane(alertArea); 
		alertArea.setEditable(false);
		southPanel1.add(scrollAlert);
		JPanel southPanel2 = new JPanel(new BorderLayout());
		southPanel2.setBorder(BorderFactory.createTitledBorder("Navegador"));
		navArea = new JTextArea(12, 20);
		scrollNav = new JScrollPane(navArea); 
		navArea.setEditable(false);
		JPanel southPanel21 = new JPanel(new GridLayout(1, 4, 0, 0));
		JPanel southPanel211 = new JPanel(new BorderLayout());
		JPanel southPanel212 = new JPanel(new BorderLayout());
		JPanel southPanel213 = new JPanel(new BorderLayout());
		JPanel southPanel214 = new JPanel(new BorderLayout());
		southPanel211.setBorder(BorderFactory.createTitledBorder("Distancia total"));
        lDistanciaTotal = new JLabel("--", SwingConstants.CENTER);
		southPanel211.add(lDistanciaTotal);
		southPanel212.setBorder(BorderFactory.createTitledBorder("Distancia restante"));
        lDistanciaRest = new JLabel("--", SwingConstants.CENTER);
		southPanel212.add(lDistanciaRest);
		southPanel213.setBorder(BorderFactory.createTitledBorder("Duracion total"));
        lDuracionTotal = new JLabel("--", SwingConstants.CENTER);
		southPanel213.add(lDuracionTotal);
		southPanel214.setBorder(BorderFactory.createTitledBorder("Duracion restante"));
        lDuracionRest = new JLabel("--", SwingConstants.CENTER);
		southPanel214.add(lDuracionRest);
		southPanel21.add(southPanel211);
		southPanel21.add(southPanel212);
		southPanel21.add(southPanel213);
		southPanel21.add(southPanel214);
		southPanel2.add(southPanel21, BorderLayout.NORTH);
		southPanel2.add(scrollNav);
		southPanel.add(southPanel1);
		southPanel.add(southPanel2);
		return southPanel;
	}
	
	/**
	 * This method creates the JFXPanel to show the WebView.
	 * 
	 * @return JFXPanel for the JavaFX elements.
	 */
	private Component mapView() {
		panel = new JFXPanel();
		return panel;
	}
	
	/**
	 * Creating the Scene means to create and set the WebView into the JFXPanel.
	 */
	private void createScene() {
		Platform.runLater(() -> {
            WebView webView = new WebView();
            webEngine = webView.getEngine();
            panel.setScene(new Scene(webView));
        });
	}
	
	/**
	 * This method reloads the map of the WebView.
	 */
	public void reload() {
		try {
			candado.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, INTERRUPTED, e);
            Thread.currentThread().interrupt();
		}
		loaded = false;
		candado.release();
		loadURL();
	}
	
	/**
	 * This method loads the map in the WebView. To avoid problems with the Google Maps API of JavaScript,
	 * the method uses a listener to unlock a Semaphore when the page is 100% loaded.
	 */
	private void loadURL() {
		try {
			candado.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, INTERRUPTED, e);
            Thread.currentThread().interrupt();
		}
		Platform.runLater(() -> {
            webEngine.load(mapURL);
            webEngine.getLoadWorker().progressProperty().addListener((arg0, arg1, arg2) -> {
                if (arg2.equals(1.0)) {
                    stop.release();
                    loaded = true;
                }
            });
        });
		candado.release();
	}
	
	/**
	 * The thread that need to interact with the WebView will check if the HTML is completely loaded with this class.
	 */
	private void check() {
		if (!loaded) {
            stop.tryAcquire();
		}
	}
	
	/**
	 * This method interacts with the HTML web page and adds a marker using a JavaScript function in the HTML file.
	 * The funtion sends the command to create the marker to the JavaFX thread.
	 * 
	 * @param lat Double latitude of the marker.
	 * @param lng Double longitude of the marker.
	 */
	public void addPointer(double lat, double lng) {
		try {
			candado.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, INTERRUPTED, e);
            Thread.currentThread().interrupt();
		}
		check();
		Platform.runLater(() -> webEngine.executeScript("createMarker("+lat+", "+lng+");"));
		candado.release();
	}
	
	/**
	 * This method interacts with the HTML web page and updates a existing marker using a JavaScript function in the
	 * HTML file. The funtion sends the command to update the marker to the JavaFX thread.
	 * 
	 * @param lat Double latitude of the marker.
	 * @param lng Double longitude of the marker.
	 */
	public void updatePointer(double lat, double lng) {
		try {
			candado.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, INTERRUPTED, e);
            Thread.currentThread().interrupt();
		}
		if (loaded) {
			Platform.runLater(() -> webEngine.executeScript("changePositionMarker("+lat+", "+lng+");"));
		}
		candado.release();
	}
	
	/**
	 * This method interacts with the HTML web page and moves the mark to a position using a JavaScript function in
	 * the HTML file. The funtion sends the command to move the map to the JavaFX thread.
	 * 
	 * @param lat Double latitude of the marker.
	 * @param lng Double longitude of the marker.
	 */
	public void updateMap(double lat, double lng) {
		try {
			candado.acquire();
		} catch (InterruptedException e) {
            log.log(Level.WARNING, INTERRUPTED, e);
            Thread.currentThread().interrupt();
		}
		check();
		Platform.runLater(() -> webEngine.executeScript("updateMap("+lat+", "+lng+");"));
		candado.release();
	}
	
	/**
	 * This method adds a text line to the alert text area.
	 * 
	 * @param text String text to write in the text area.
	 */
	public void addAlertText(String text) {
		alertArea.append("ALERTA: "+text+"\n");
	}
	
	/**
	 * This method adds a text line to the navigation text area.
	 * 
	 * @param text String text to write in the text area.
	 */
	public void addNavText(String text) {
		navArea.append("NAVEGADOR: "+text+"\n");
	}
	
	/**
	 * This method cclears the navigation text area.
	 */
	public void clearNavText() {
		navArea.setText("");
	}
	
	public void setID(int id) {
		lID.setText(String.valueOf(id));
	}
	
	/**
	 * Sets the state JLabel depending on the state number.
	 * 
	 * @param estado Integer state number.
	 */
	public void setEstado(int estado) {
		if (estado == 0) {
			lEstado.setText("LIBRE");
		} else if (estado == 1) {
			lEstado.setText("VIAJE IDA");
		} else if (estado == 2) {
			lEstado.setText("VIAJE VUELTA");
		}
	}
	
	public void setLocation(double lat, double lng) {
		lLat.setText("LAT: "+lat);
		lLng.setText("LNG: "+lng);
	}
	
	public void setURL(String url) {
		lURL.setText(url);
	}
	
	public void setSocket(int socket) {
		lSocket.setText(String.valueOf(socket));
	}

	/**
	 * Resets all the JLabels at the north panel.
	 */
	public void resetNav() {
		lDuracionTotal.setText("--");
		lDuracionRest.setText("--");
		lDistanciaTotal.setText("--");
		lDistanciaRest.setText("--");
	}
	
	public void setDuracionTotal(String text) {
		lDuracionTotal.setText(text);
	}
	
	public void setDuracionRest(String text) {
		lDuracionRest.setText(text);
	}
	
	public void setDistanciaTotal(String text) {
		lDistanciaTotal.setText(text);
	}
	
	public void setDistanciaRest(String text) {
		lDistanciaRest.setText(text);
	}
	
}
