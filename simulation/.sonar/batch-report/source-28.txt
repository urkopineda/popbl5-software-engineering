package utils;

import data.Definitions;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConfigFile
 * 
 * This class reads and writes the config file.
 * 
 * @author Skynet Team
 *
 */
public class ConfigFile {

	private static final Logger log = Logger.getLogger(ConfigFile.class.getName());
	private Definitions definitions;

	public ConfigFile(Definitions definitions) {
		this.definitions = definitions;
	}
	
	public boolean check() {
		File conf = new File(definitions.getConfigFile());
		return conf.exists();
	}

	public void writeAll() {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(definitions.getConfigFile());
		    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            fileWriter.close();
			bufferedWriter.write("socketNumber="+definitions.getSocketNumber());
			bufferedWriter.newLine();
			bufferedWriter.write("serverURL="+definitions.getSocketAddres());
			bufferedWriter.newLine();
			bufferedWriter.write("ID="+definitions.getId());
			bufferedWriter.newLine();
			bufferedWriter.write("ESTADO="+definitions.getEstado());
			bufferedWriter.newLine();
			bufferedWriter.write("LAT="+definitions.getLat());
			bufferedWriter.newLine();
			bufferedWriter.write("LNG="+definitions.getLng());
			bufferedWriter.newLine();
			bufferedWriter.close();
		} catch (IOException | NullPointerException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
		}
    }
	
	public String readAll() {
		try {
            BufferedReader bufferedReader = openFileReader();
            String line;
			while ((line = bufferedReader.readLine()) != null) {
				String [] data = line.split("=");
				switch (data[0]) {
					case "socketNumber": definitions.setSocketNumber(Integer.parseInt(data[1]));
						break;
					case "serverURL": definitions.setSocketAddres(data[1]);
						break;
					case "ID": definitions.setId(Integer.parseInt(data[1]));
						break;
					case "ESTADO": definitions.setEstado(Integer.parseInt(data[1]));
						break;
					case "LAT": definitions.setLat(Double.parseDouble(data[1]));
						break;
					case "LNG": definitions.setLng(Double.parseDouble(data[1]));
						break;
					default: break;
				}
			}
            closeFileReader(bufferedReader);
		} catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "ERROR: El archivo no existe, utiliza el metodo 'write' para escribirlo antes.", e);
        } catch (NullPointerException | IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
	}

    private BufferedReader openFileReader() throws IOException {
        FileReader fileReader = new FileReader(definitions.getConfigFile());
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        fileReader.close();
        return bufferedReader;
    }

    private void closeFileReader(BufferedReader bufferedReader) throws IOException {
        bufferedReader.close();
    }
	
}
