package json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import simulation.Navigation;
import simulation.Step;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JSON {

    private static final Logger log = Logger.getLogger(JSON.class.getName());
    private String data;
	private JSONObject obj;
	private Navigation nav;
    private static final String DISTANCE = "distance";
    private static final String DURATION = "duration";
    private static final String START_LOCATION = "start_location";
    private static final String TEXT = "text";
    private static final String VALUE = "value";
    private static final String LAT = "lat";
    private static final String LNG = "lng";


    /**
	 * This constructor creates a JSON object from a JSON in a string.
	 * 
	 * @param data The JSON in a String.
	 */
	public JSON(String data) {
		this.data = data;
	}
	
	/**
	 * This methods parses the JSON to "Navigation" and "Step" objects. To do that, takes the JSON
	 * and extracts the general information of the JSON into a "Navigation" object. Then, takes the
	 * "steps" array from the JSON and creates "Step" objects with the elements on the array.
	 * Finally, adds the "Step" object to the ArrayList of "Step" at the created "Navigation" object.
	 * 
	 * @return Integer result code (ERROR: -1 | SUCCESS: 0)
	 */
	public int parseNav() throws JSONException {
		try {
			obj = new JSONObject(data);
			if ("OK".equals(obj.getString("status"))) {
				nav = new Navigation();
				JSONObject legs = obj.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0);
				nav.setDistance(legs.getJSONObject(DISTANCE).getString(TEXT));
				nav.setDuration(legs.getJSONObject(DURATION).getString(TEXT));
				JSONArray steps = legs.getJSONArray("steps");
				for (int i = 0; i < steps.length(); i++) {
					JSONObject tempStep = steps.getJSONObject(i);
					Step step = new Step();
					step.setDistanceV(tempStep.getJSONObject(DISTANCE).getInt(VALUE));
					step.setDurationV(tempStep.getJSONObject(DURATION).getInt(VALUE));
					step.setStartLat(tempStep.getJSONObject(START_LOCATION).getDouble(LAT));
					step.setStartLng(tempStep.getJSONObject(START_LOCATION).getDouble(LNG));
					step.setInstruction(tempStep.getString("html_instructions"));
					nav.addStep(step);
				}
			} else {
                log.log(Level.WARNING, "JSON - ERROR: Estado de ruta no válido.");
            }
            return 0;
		} catch (JSONException e) {
            log.log(Level.SEVERE, "JSON - ERROR: JSON no válido.");
            throw new JSONException(e);
		}
	}
	
	public Navigation getNav() {
		return nav;
	}
	
}
